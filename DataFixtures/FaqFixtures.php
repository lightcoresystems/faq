<?php

namespace LightCore\FaqBundle\DataFixtures;

use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use LightCore\FaqBundle\Entity\Faq;

class FaqFixtures extends Fixture
{
    /**
     * @var Slugify
     */
    private $slugify;

    /**
     * FaqFixtures constructor.
     */
    public function __construct()
    {
        $this->slugify = new Slugify();
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 2; $i++) {
            $faq = new Faq();
            $faq->setDefaultLocale('pl');
            $faq->setContent('TREŚĆ ' . $i);
            $faq->setTitle('TYTUŁ ' . $i);
            $faq->setSlug($this->slugify->slugify($faq->getTitle()));
            $faq->setMetaTitle('META TYTUL ' . $i);
            $faq->setMetaDesc('OPIS MEGA ' . $i);
            $faq->setMetaKeywords('SLOWA KLUCZOWE' . $i);
            $manager->persist($faq);
            $faq->mergeNewTranslations();
        }

        $manager->flush();
    }
}
