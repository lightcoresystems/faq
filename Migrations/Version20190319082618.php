<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190319082618 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE faq_translation (id INT AUTO_INCREMENT NOT NULL, translatable_id INT DEFAULT NULL, slug VARCHAR(255) NOT NULL, title VARCHAR(255) NOT NULL, meta_title VARCHAR(255) NOT NULL, meta_desc VARCHAR(255) NOT NULL, meta_keywords LONGTEXT DEFAULT NULL, content LONGTEXT NOT NULL, locale VARCHAR(255) NOT NULL, INDEX IDX_50A668562C2AC5D3 (translatable_id), UNIQUE INDEX faq_translation_unique_translation (translatable_id, locale), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE faq_translation ADD CONSTRAINT FK_50A668562C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES faq (id) ON DELETE CASCADE');
        $this->addSql('DROP INDEX UNIQ_E8FF75CC989D9B62 ON faq');

        $this->addSql("insert into faq_translation (translatable_id, slug, title, meta_title, meta_desc, meta_keywords, content, locale) select id, slug, title, meta_title, meta_desc, meta_keywords, content, 'pl' from faq;");

        $this->addSql('ALTER TABLE faq DROP slug, DROP title, DROP content, DROP meta_title, DROP meta_desc, DROP meta_keywords');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE faq_translation');
        $this->addSql('ALTER TABLE faq ADD slug VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD content LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, ADD meta_title VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD meta_desc VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, ADD meta_keywords LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E8FF75CC989D9B62 ON faq (slug)');
    }
}
