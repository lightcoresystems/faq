<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181011184320 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE faq CHANGE meta_title meta_title VARCHAR(255) NOT NULL, CHANGE meta_desc meta_desc VARCHAR(255) NOT NULL, CHANGE meta_keywords meta_keywords LONGTEXT NOT NULL');
        $this->addSql('DROP INDEX uniq_6747643b989d9b70 ON faq; CREATE UNIQUE INDEX UNIQ_E8FF75CC989D9B62 ON faq (slug);');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE faq CHANGE meta_title meta_title VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE meta_desc meta_desc VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, CHANGE meta_keywords meta_keywords LONGTEXT DEFAULT NULL COLLATE utf8mb4_unicode_ci');
        $this->addSql('DROP INDEX UNIQ_E8FF75CC989D9B62 ON faq; CREATE UNIQUE INDEX uniq_6747643b989d9b70 ON faq (slug);');
    }
}
