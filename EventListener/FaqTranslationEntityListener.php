<?php

namespace LightCore\FaqBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Cocur\Slugify\Slugify;
use LightCore\FaqBundle\Entity\FaqTranslation;
use LightCore\FaqBundle\Services\UniqueSlugGenerator;
use Symfony\Component\HttpFoundation\RequestStack;

class FaqTranslationEntityListener
{
    /**
     * @var Slugify
     */
    private $slugify;

    /**
     * @var UniqueSlugGenerator
     */
    private $slugGenerator;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param UniqueSlugGenerator $slugGenerator
     */
    public function __construct(UniqueSlugGenerator $slugGenerator, RequestStack $requestStack)
    {
        $this->slugify = new Slugify();
        $this->slugGenerator = $slugGenerator;
        $this->requestStack = $requestStack;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->update($entity);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->update($entity);
    }

    private function update($entity)
    {
        if (!$entity instanceof FaqTranslation) {
            return;
        }

        if (empty($entity->getSlug()) && !empty($entity->getTitle())) {
            $entity->setSlug(
                $this->slugGenerator->uniqueToken(
                    $this->slugify->slugify($entity->getTitle()),
                    $this->requestStack->getCurrentRequest()->getLocale()
                )
            );
        }

        if (empty($entity->getMetaTitle()) && !empty($entity->getTitle())) {
            $entity->setMetaTitle($entity->getTitle());
        }
    }
}