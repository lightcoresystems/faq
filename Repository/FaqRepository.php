<?php

namespace LightCore\FaqBundle\Repository;

use LightCore\FaqBundle\Entity\Faq;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use LightCore\FaqBundle\Entity\FaqTranslation;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Faq|null find($id, $lockMode = null, $lockVersion = null)
 * @method Faq|null findOneBy(array $criteria, array $orderBy = null)
 * @method Faq[]    findAll()
 * @method Faq[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FaqRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faq::class);
    }

    /**
     * @param string $slug
     * @param string $locale
     * @return FaqTranslation|null
     */
    public function findOneBySlugAndLocale(string $slug, string $locale): ?FaqTranslation
    {
        $dql = "SELECT ft FROM LightCore\FaqBundle\Entity\FaqTranslation ft
            WHERE ft.slug = :slug AND ft.locale = :locale";

        $query = $this->getEntityManager()->createQuery($dql);
        $query->setParameters([
            'locale' => $locale,
            'slug' => $slug
        ]);
        $query->setMaxResults(1);

        return $query->getOneOrNullResult();
    }
}
