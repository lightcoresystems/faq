<?php

namespace LightCore\FaqBundle\Form;

use A2lix\TranslationFormBundle\Form\Type\TranslationsType;
use LightCore\FaqBundle\Entity\Faq;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class FaqType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', TranslationsType::class, [
                'fields' => [
                    'slug' => [
                        'help' => 'Kod ten będzie widoczny w adresie do strony statycznej'
                    ],
                    'title' => [
                        'constraints' => [
                            new NotBlank([
                                'groups' => ['add', 'edit'],
                                'message' => 'Title can not be empty'
                            ])
                        ]
                    ],
                    'content' => [
                        'field_type' => CKEditorType::class,
                        'constraints' => [
                            new NotBlank([
                                'groups' => ['add', 'edit'],
                                'message' => 'Content can not be empty'
                            ])
                        ]
                    ],
                    'metaTitle' => [
                        'help' => 'This title is shown on the top of browser and send to searchers like Google.'
                    ],
                    'metaDesc' => [
                        'help' => 'This description is used in searchers like Google. Max length is 160 chars.'
                    ],
                    'metaKeywords' => [
                        'help' => 'Please type keywords describing this page, separated by semicolon.'
                    ]
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Faq::class,
            'translation_domain' => 'LightCoreFaqBundle'
        ]);
    }
}
