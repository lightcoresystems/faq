(($) => {
    $(() => {
        $('.faq-panel.panel')
            .on('click', '.panel-heading:not(.selected)', function () {
                let target = $(this).data('target');
                let panel = $(this).closest('.faq-panel');
                $(panel).find('.panel-body').hide(500);
                $(panel).find('.panel-heading.selected').removeClass('selected');
                $(target).show(500);
                $(this).addClass('selected');
            })
        ;
    });
})(jQuery);