<?php

namespace LightCore\FaqBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use LightCore\FaqBundle\Entity\FaqTranslation;
use LightCore\FaqBundle\Repository\FaqRepository;

class UniqueSlugGenerator
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var FaqRepository
     */
    private $faqRepository;

    public function __construct(EntityManagerInterface $entityManager, FaqRepository $faqRepository)
    {
        $this->entityManager = $entityManager;
        $this->faqRepository = $faqRepository;
    }

    /**
     * @param string $slug
     * @param string $locale
     * @return string
     */
    public function uniqueToken(string $slug = null, string $locale): string
    {
        $iteration = 1;
        if ($slug == null) {
            $slug = substr(md5(uniqid() . uniqid() . time()), 0, 15);
        }

        $baseSlug = $slug;
        while ($this->faqRepository->findOneBySlugAndLocale($slug, $locale) instanceof FaqTranslation) {
            $slug = $baseSlug . '-' . ++$iteration;
        }

        return $slug;
    }
}