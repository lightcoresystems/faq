<?php

namespace LightCore\FaqBundle\Controller;

use App\Services\VarnishService;
use LightCore\FaqBundle\Repository\FaqRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    /**
     * @param FaqRepository $repository
     * @param VarnishService $varnishService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(FaqRepository $repository, VarnishService $varnishService)
    {
        $faqList = $repository->findAll();

        return $varnishService->setCache($this->render($this->getParameter('light_core_faq.templates.show'), [
            'faqList' => $faqList
        ]));
    }
}