<?php

namespace LightCore\FaqBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use LightCore\FaqBundle\Entity\Faq;
use LightCore\FaqBundle\Entity\FaqTranslation;
use LightCore\FaqBundle\Form\FaqType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * StaticContentController constructor.
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     */
    public function __construct(EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {
        $this->entityManager = $entityManager;
        $this->paginator = $paginator;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function list(Request $request)
    {
        $query = $this->entityManager->createQuery(
            "SELECT faq FROM " . Faq::class . " faq " .
            "LEFT JOIN " . FaqTranslation::class . " ft WITH faq.id = ft.translatable AND ft.locale = :locale"
        )->setParameter('locale', $request->getLocale());

        $pagination = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@LightCoreFaq/admin/list.html.twig', ['pagination' => $pagination]);
    }

    /**
     * @param Faq $faq
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function remove(Faq $faq)
    {
        $this->entityManager->remove($faq);
        $this->entityManager->flush();
        $this->addFlash('success', 'Wpis FAQ został usunięty.');
        return $this->redirectToRoute('lightcore_faq_list');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function add(Request $request) {
        $faq = new Faq();
        $form = $this->createForm(FaqType::class, $faq, [
            'validation_groups' => ['add']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($faq);
            $this->entityManager->flush();
            $this->addFlash('success', 'Wpis FAQ został dodany');
            return $this->redirect($this->generateUrl('lightcore_faq_edit', ['id' => $faq->getId()]));
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Znaleziono błędy podczas zapisu');
        }

        return $this->render('@LightCoreFaq/admin/add.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param Faq $faq
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(
        Request $request,
        Faq $faq
    ) {
        $form = $this->createForm(FaqType::class, $faq, [
            'validation_groups' => ['edit']
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($faq);
            $this->entityManager->flush();
            $this->addFlash('success', 'Strona statyczna została zmodyfikowana');
            return $this->redirect($this->generateUrl('lightcore_faq_edit', ['id' => $faq->getId()]));
        } elseif ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Znaleziono błędy podczas zapisu');
        }

        return $this->render('@LightCoreFaq/admin/edit.html.twig', [
            'form' => $form->createView(),
            'faq' => $faq
        ]);
    }
}
