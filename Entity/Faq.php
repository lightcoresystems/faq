<?php

namespace LightCore\FaqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\TranslatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity(repositoryClass="LightCore\FaqBundle\Repository\FaqRepository")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 * @method string getSlug()
 * @method self setSlug(string $slug)
 * @method string getTitle()
 * @method self setTitle(string $title)
 * @method string getContent()
 * @method self setContent(string $content)
 * @method string getMetaTitle()
 * @method self setMetaTitle(string $metaTitle)
 * @method string getMetaDesc()
 * @method self setMetaDesc(string $metaDesc)
 * @method string getMetaKeywords()
 * @method self setMetaKeywords(string $metaKeywords)
 */
class Faq implements TranslatableInterface
{
    use ORMBehaviors\Translatable\TranslatableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @var integer
     */
    private $id;

    /**
     * @ORM\OneToMany(
     *     targetEntity="LightCore\FaqBundle\Entity\FaqTranslation",
     *     orphanRemoval=true,
     *     mappedBy="translatable",
     *     indexBy="locale",
     *     cascade={"persist", "merge", "remove"}
     *)
     * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
     */
    protected $translations;

    public function getId(): int
    {
        return $this->id;
    }

    public function __call($method, $arguments)
    {
        if (!method_exists(self::getTranslationEntityClass(), $method)) {
            $method = 'get' . ucfirst($method);
        }

        return $this->proxyCurrentLocaleTranslation($method, $arguments);
    }
}
