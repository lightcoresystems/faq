<?php

namespace LightCore\FaqBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('light_core_faq');
        $builder->getRootNode()
            ->children()
                ->arrayNode('templates')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('show')->defaultValue('@LightCoreFaq/front/show.html.twig')->end()
                    ->end()
                ->end()
            ->end();
        return $builder;
    }
}
